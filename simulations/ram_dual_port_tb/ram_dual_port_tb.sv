`timescale 1ns/1ps

module ram_dual_port_tb ();

`define     NS_PER_US       1000
`define     US_PER_MS       1000
`define     MS_PER_S        1000
`define     NS_PER_MS       `NS_PER_US * `US_PER_MS
`define     US_PER_S        `US_PER_MS * `MS_PER_S
`define     NS_PER_S        `NS_PER_MS * `MS_PER_S


localparam  CLK_PERIOD_NS   = 20;

localparam  data_width      = 32;
localparam  ram_depth       = 256;
localparam  addr_width      = $clog2(ram_depth);

reg                         clk;
reg                         rst;
reg                         wren;
reg                         rden;
reg     [addr_width-1:0]    wr_addr;
reg     [addr_width-1:0]    rd_addr;
reg     [data_width-1:0]    data;
wire    [data_width-1:0]    q;

reg     [31:0]              ticks;

reg     [data_width-1:0]    r_check_ram     [ram_depth-1:0];
wire                        s_read_while_write;

initial begin
    clk     <= 1'b0;
    rst     <= 1'b1;
    ticks   <= {32{1'b0}};
end

initial begin
    wren    <= 1'b0;
    rden    <= 1'b0;
    wr_addr <= {addr_width{1'b0}};
    rd_addr <= {addr_width{1'b0}};
    data    <= {data_width{1'b0}};
end

initial begin
    int                     i;

    for (i=0; i < ram_depth; i = i+1) begin
        r_check_ram[i] <= {data_width{1'b0}};
    end
end

always 
begin
    #(CLK_PERIOD_NS/2) 
    clk = !clk;
end

always @(posedge clk)
begin
    ticks <= ticks + 1'b1;
end

function void write_to_ram;
    input   [addr_width-1:0]    addr;
    input   [data_width-1:0]    in_data;
begin
    wr_addr <= addr;
    data <= in_data;
    wren <= 1'b1;
end
endfunction

function void read_from_ram;
    input   [addr_width-1:0]    addr;
begin
    rd_addr <= addr;
    rden <= 1'b1;
end
endfunction

always @(posedge clk)
begin
    case (ticks)
        1: 
        begin
            write_to_ram({{addr_width-4{1'b0}},4'd10}, {data_width/4{4'hA}});
        end
        2: 
        begin
            wren <= 1'b0;
        end
        10:
        begin
            rst <= 1'b0;
        end
        20:
        begin
            write_to_ram({{addr_width-4{1'b0}},4'd0}, {data_width/4{4'h5}});
        end
        21: 
        begin
            wren <= 1'b0;
        end
        30:
        begin
            read_from_ram({{addr_width-4{1'b0}}, 4'd10});
        end
        31: 
        begin
            rden <= 1'b0;
        end
        32:
        begin
            read_from_ram({{addr_width-4{1'b0}}, 4'd0});
        end
        33: 
        begin
            rden <= 1'b0;
        end
        34:
        begin
            write_to_ram({{addr_width-4{1'b0}}, 4'd3}, {data_width/4{4'h2}});
            read_from_ram({{addr_width-4{1'b0}}, 4'd3});
        end
        35: 
        begin
            wren <= 1'b0;
        end
        36:
        begin
            rden <= 1'b0;
        end
        100:
        $finish;
        default:
        begin
        end
    endcase
end

// Checker functionality
reg     [data_width-1:0]        r_check_q;

initial begin
    r_check_q <= {data_width{1'b0}};
end

always @(posedge clk)
begin
    if (wren & !rst) begin
        r_check_ram[wr_addr] <= data;
    end
end

always @(posedge clk)
begin
    if (rden & !rst) begin
        r_check_q <= r_check_ram[rd_addr];
    end
end

always @(posedge clk)
begin
    if (r_check_q != q) begin
        $error("%0d: RAM does not match functionality, checker = %H, actual = %H", 
            ticks, 
            r_check_q,
            q);
    end
end

ram_dual_port
#(
    .data_width     (data_width),
    .ram_size       (ram_depth)
) inst_dut (
    .clk            (clk),
    .rst            (rst),
    .wren           (wren),
    .rden           (rden),
    .wr_addr        (wr_addr),
    .rd_addr        (rd_addr),
    .data           (data),
    .q              (q)
);


endmodule
