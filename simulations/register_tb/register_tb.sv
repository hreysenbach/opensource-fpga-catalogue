`timescale 1ns/1ps

module register_tb ();

`define     NS_PER_US       1000
`define     US_PER_MS       1000
`define     MS_PER_S        1000
`define     NS_PER_MS       `NS_PER_US * `US_PER_MS
`define     US_PER_S        `US_PER_MS * `MS_PER_S
`define     NS_PER_S        `NS_PER_MS * `MS_PER_S


localparam  CLK_PERIOD_NS   = 20;

localparam  data_width      = 8;

reg                         clk;
reg                         rst;
reg                         en;
reg     [data_width-1:0]    data;
wire    [data_width-1:0]    out;

reg     [data_width-1:0]    r_check_reg;

reg     [31:0]              ticks;

initial begin
    clk     <= 1'b0;
    rst     <= 1'b1;
    ticks   <= {32{1'b0}};
end

initial begin
    en <= 1'b0;
    data <= {data_width{1'b0}};
end

always 
begin
    #(CLK_PERIOD_NS/2) 
    clk = !clk;
end

always @(posedge clk)
begin
    ticks <= ticks + 1'b1;
end

always @(posedge clk)
begin
    if (en & !rst) begin
        r_check_reg <= data;
    end
end

always @(posedge clk)
begin
    if (out != r_check_reg) begin
        $error("%0d: output does not match r_check_reg, r_check_reg = %H ouput = %H",
            ticks,
            r_check_reg,
            out
        );
    end
end

always @(posedge clk)
begin
    case (ticks)
        10:
        begin
            rst <= 1'b0;
        end
        12: 
        begin
            en <= 1'b1;
            data <= {data_width/4{4'h5}};
        end
        20:
        begin
            en <= 1'b1;
            data <= {data_width/4{4'hA}};
        end
        21:
        begin
            en <= 1'b1;
            data <= {data_width/4{4'h5}};
        end
        22:
        begin
            en <= 1'b1;
            data <= {data_width/4{4'h5}};
        end
        100:
        begin
            $finish;
        end
        default:
        begin
            en <= 1'b0;
        end
    endcase
end

register
#(
    .data_width         (data_width)
) inst_dut (
    .clk                (clk),
    .rst                (rst),
    .en                 (en),
    .d                  (data),
    .q                  (out)
);


endmodule
