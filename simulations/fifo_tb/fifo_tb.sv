`timescale 1ns/1ps

module fifo_tb ();

`define     NS_PER_US       1000
`define     US_PER_MS       1000
`define     MS_PER_S        1000
`define     NS_PER_MS       `NS_PER_US * `US_PER_MS
`define     US_PER_S        `US_PER_MS * `MS_PER_S
`define     NS_PER_S        `NS_PER_MS * `MS_PER_S


localparam  CLK_PERIOD_NS   = 20;

localparam  data_width      = 32;
localparam  number_of_words = 256;
localparam  addr_width      = $clog2(number_of_words);

reg                         clk;
reg                         rst;
reg                         wren;
reg                         rden;
reg     [data_width-1:0]    data;
wire    [data_width-1:0]    q;
wire                        empty;
wire                        full;
wire                        underflow;
wire                        overflow;
wire    [addr_width-1:0]    num_words;

reg     [31:0]              ticks;

reg     [addr_width-1:0]    r_num_words;
wire                        s_empty;
wire                        s_full;

initial begin
    clk     <= 1'b0;
    rst     <= 1'b1;
    ticks   <= {32{1'b0}};
end

initial begin
    wren    <= 1'b0;
    rden    <= 1'b0;
    data    <= {data_width{1'b0}};
end

always 
begin
    #(CLK_PERIOD_NS/2) 
    clk = !clk;
end

always @(posedge clk)
begin
    ticks <= ticks + 1'b1;
end

// rst
always @(posedge clk)
begin
    case (ticks)
        10:
            rst <= 1'b0;
        18:
            rst <= 1'b1;
        19:
            rst <= 1'b0;
        1024:
            $finish;
        default:
        begin
        end
    endcase
end

function void write_to_fifo;
    input   [data_width-1:0]    d;
begin
    wren <= 1'b1;
    data <= d;
end
endfunction

function void read_from_fifo;
begin
    rden <= 1'b1;
end
endfunction

always @(posedge clk)
begin
    rden <= 1'b0;
    wren <= 1'b0;

    if (ticks == 12) begin
        write_to_fifo({data_width/4{4'hA}});
    end else if (ticks == 14) begin
        read_from_fifo();
    end else if (ticks == 16) begin
        read_from_fifo();
    end else if (ticks == 20) begin
        write_to_fifo({data_width/4{4'h5}});
        read_from_fifo();
    end else if (ticks > 128 && ticks < 512) begin
        if ((ticks % 2) == 0) begin
            write_to_fifo({data_width/4{4'hA}});
        end else begin
            write_to_fifo({data_width/4{4'h5}});
        end
    end
end

fifo
#(
    .data_width         (data_width),
    .number_of_words    (number_of_words)
) inst_dut (
    .clk                (clk),
    .rst                (rst),
    .wren               (wren),
    .rden               (rden),
    .d                  (data),
    .q                  (q),
    .empty              (empty),
    .full               (full),
    .underflow          (underflow),
    .overflow           (overflow),
    .num_words          (num_words)
);


endmodule
