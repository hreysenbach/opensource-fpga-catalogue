set DUT     fifo

array set dependencies {
    register
    ram_dual_port
}

set DUT_TB  ${DUT}_tb
set ROOTDIR ../..
set SRC_DIR $ROOTDIR/verilog
set SIM_DIR $ROOTDIR/simulations/$DUT_TB

if {[file exists rtl_work]} {
    vdel -lib rtl_work -all
}

vlib        rtl_work
vmap work   rtl_work

foreach {dependency} [array get dependencies] {
    vlog -work work +define+SIMULATION      $SRC_DIR/$dependency.v
}

vlog -work work     +define+SIMULATION      $SRC_DIR/$DUT.v
vlog -work work -sv +define+SIMULATION      $SIM_DIR/$DUT_TB.sv

vsim -t 1ps -fsmdebug -L work -voptargs="+acc" $DUT_TB

config wave -namecolwidth 200 -valuecolwidth 100

add wave -group "Clocks and Reset"  -radix unsigned     /${DUT_TB}/ticks
add wave -group "Clocks and Reset"                      /${DUT_TB}/clk
add wave -group "Clocks and Reset"                      /${DUT_TB}/rst

add wave -group "Enables"                               /${DUT_TB}/rden
add wave -group "Enables"                               /${DUT_TB}/wren

add wave -group "Data"              -hex                /${DUT_TB}/data
add wave -group "Data"              -hex                /${DUT_TB}/q

add wave -group "Status"                                /${DUT_TB}/empty
add wave -group "Status"                                /${DUT_TB}/full
add wave -group "Status"                                /${DUT_TB}/underflow
add wave -group "Status"                                /${DUT_TB}/overflow
add wave -group "Status"            -radix unsigned     /${DUT_TB}/num_words

add wave -group "Internal"                              /${DUT_TB}/inst_dut/s_read_while_write
add wave -group "Internal"                              /${DUT_TB}/inst_dut/s_underflow
add wave -group "Internal"                              /${DUT_TB}/inst_dut/r_underflow
add wave -group "Internal"                              /${DUT_TB}/inst_dut/s_overflow
add wave -group "Internal"                              /${DUT_TB}/inst_dut/r_overflow
add wave -group "Internal"          -radix unsigned     /${DUT_TB}/inst_dut/r_num_words
add wave -group "Internal"          -radix unsigned     /${DUT_TB}/inst_dut/r_read_ptr
add wave -group "Internal"                              /${DUT_TB}/inst_dut/s_read_ptr_incr
add wave -group "Internal"          -radix unsigned     /${DUT_TB}/inst_dut/r_write_ptr
add wave -group "Internal"                              /${DUT_TB}/inst_dut/s_write_ptr_incr

run -all
