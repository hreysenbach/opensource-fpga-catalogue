library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clk_gen is
    generic (
        half_period         : time := 20 ns
    );
    port (
        clk                 : out   std_logic := '0';
        ticks               : out   unsigned(31 downto 0) := (others => '0')
    );
end entity;

architecture behavioural of clk_gen is
    signal clk_sig          : std_logic := '0';
    signal ticks_sig        : integer := 0;
begin
    clk <= clk_sig;
    ticks <= to_unsigned(ticks_sig, 32);

    process
    begin
        wait for half_period;
        clk_sig <= not clk_sig;
    end process;

    process (clk_sig)
    begin
        if (rising_edge(clk_sig)) then
            ticks_sig <= ticks_sig + 1;
        end if;
    end process;

end architecture;
