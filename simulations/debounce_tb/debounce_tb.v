module debounce_tb();

reg         clk;
reg         in;
wire        out;

integer     ticks;

debounce # (
    .debounce_target(50)
    ) dut (
        .clk(clk), 
        .in(in), 
        .out(out)
    );

initial begin
    ticks <= 0;
    clk <= 1'b0;
    in <= 1'b0;
    forever begin
        #1 clk = ~clk;
    end
end


always @(posedge clk) begin
    ticks <= ticks + 1;
    case(ticks)
        200:
            in <= 1'b1;
        250:
            in <= 1'b0;
        275:
            in <= 1'b1;
        400:
            in <= 1'b0;
    endcase
end

endmodule
