module register (
    clk,
    rst,
    en,
    d,
    q
);

parameter data_width                = 8;

input   wire                        clk;
input   wire                        rst;
input   wire                        en;
input   wire    [data_width-1:0]    d;
output  reg     [data_width-1:0]    q;

`ifdef SIMULATION
    initial begin
        q <= {data_width{1'b0}};
    end
`endif

always @(posedge clk)
begin
    if (rst == 1'b1) begin
        q <= {data_width{1'b0}};
    end else if (en == 1'b1) begin
        q <= d;
    end
end

endmodule
