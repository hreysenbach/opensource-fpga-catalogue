module debounce 
#(
    parameter debounce_target = 5000000
)
(
    input clk, 
    input in, 
    output reg out
);

reg old_in;
integer debounce_count;

initial begin
    debounce_count <= 0;
    out <= in;
end

always @(posedge clk) begin
    if (old_in == in) begin
        if (debounce_count < debounce_target) begin
            debounce_count <= debounce_count + 1;
        end else begin
            out <= in;
        end
    end else begin 
        debounce_count <= 0;
    end
    old_in <= in;
end

endmodule
