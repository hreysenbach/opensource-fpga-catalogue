module trigger (
    input   wire            clk,
    input   wire            rst,
    input   wire            en,
    input   wire            sig_in,
    input   wire    [2:0]   cfg,
    output  wire            trig
);

// Different types of trigger for a single bit:
//      0       - bit is low
//      1       - bit is high
//      rise    - rising edge
//      fall    - falling edge
//      edge    - any edge

localparam STATE_ARMED      = 2'b00,
           STATE_TRIGGERED  = 2'b01,
           STATE_ERROR      = 2'b11;
localparam CONFIG_HIGH      = 3'b000,
           CONFIG_LOW       = 3'b001,
           CONFIG_RISE      = 3'b010,
           CONFIG_FALL      = 3'b011,
           CONFIG_EDGE      = 3'b100;

reg     [1:0]   state
reg             old_sig

initial 
begin
end

always @(posedge clk)
begin
    if (rst == 1'b1) begin
        state <= STATE_ARMED;
        trig <= 1'b0;
    end else begin
        if (en == 1'b1) begin
            if (state == STATE_TRIGGERED) begin
                trig <= 1'b0;
            end else begin
                case (cfg)
                    CONFIG_HIGH:
                        // bit is high
                        if (sig_in == 1'b1) begin
                            state <= STATE_TRIGGERED
                            trig <= 1'b1;
                        end
                    CONFIG_LOW:
                        // bit is low
                        if (sig_in == 1'b0) begin
                            state <= STATE_TRIGGERED
                            trig <= 1'b1;
                        end
                    CONFIG_RISE:
                        // Rising edge
                        if (sig_in == 1'b1 && old_sig == 1'b0) begin
                            state <= STATE_TRIGGERED
                            trig <= 1'b1;
                        end
                    CONFIG_FALL:
                        // Falling edge
                        if (sig_in == 1'b0 && old_sig == 1'b1) begin
                            state <= STATE_TRIGGERED
                            trig <= 1'b1;
                        end
                    CONFIG_EDGE:
                        // Any edge
                        if (sig_in != old_sig) begin
                            state <= STATE_TRIGGERED;
                            trig <= 1'b1;
                        end
                endcase
            end
            old_sig <= sig_in;
        end
    end
end

endmodule
