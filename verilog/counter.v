module counter
(
    clk,
    rst,
    en,
    out
);

parameter       data_width = 32;

input   wire                        clk;
input   wire                        rst;
input   wire                        en;
output  wire    [data_width-1:0]    out;

register # (
    .data_width (data_width)
) inst_counter_reg (
    .clk        (clk),
    .rst        (rst),
    .en         (en),
    .d          (out + 1'b1),
    .q          (out)
);
     

endmodule
