SIM_DIR = \
	simulations

.PHONY: all clean distclean

all: 
	make -C ${SIM_DIR} all

clean: 
	make -C ${SIM_DIR} clean

distclean: 
	make -C ${SIM_DIR} distclean
